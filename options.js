
document.getElementById('save').addEventListener('click', function () {
    chrome.storage.sync.set({
        url: document.getElementById('jira-url').value,
        detail: {
            expand: document.getElementById('expand-detail').checked,
            epicLink: document.getElementById('epic-link').checked
        }
    }, function () {
        window.close();
    });
});

document.getElementById('cancel').addEventListener('click', function (){
  window.close();
});

document.addEventListener('DOMContentLoaded', function () {
    chrome.storage.sync.get({
        url: '',
        detail: {
          expand: false,
          epicLink: false
        }
    }, function (items) {
        document.getElementById('jira-url').value = items.url;
        document.getElementById('expand-detail').checked = items.detail.expand;
        document.getElementById('epic-link').checked = items.detail.epicLink;
    });
});

