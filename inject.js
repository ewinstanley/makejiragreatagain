
(function() {
    'use strict';

    function makeJiraGreat() {
        var $styles = $('<style></style>').appendTo('head');

        chrome.storage.sync.get('detail', function (item) {
            if (item.detail && item.detail.expand) {
                $styles.append('#ghx-detail-view { width: 600px !important; }');

                $styles.append('#ghx-detail-contents { background: none !important; }');
                $styles.append('#ghx-detail-view .ghx-detail-nav-menu { display: none !important; }');
                $styles.append('#ghx-detail-view .ghx-project-avatar { display: none !important; }');
                $styles.append('#ghx-detail-view .ghx-sizer { display: none !important; }');
                $styles.append('#ghx-detail-view .ghx-detail-head, #ghx-detail-view .ghx-detail-nav-content { padding-left: 10px; }');
            }

            if (item.detail && item.detail.epicLink) {
                $styles
                    .append('#ghx-detail-view .js-epic-remove { padding-right: 5px !important; }')
                    .append('#ghx-detail-view .aui-iconfont-remove-label { display: none !important; }');

                $('#ghx-detail-view').bind('DOMSubtreeModified', function (e) {
                    if (e.target.innerHTML.length) {
                        var el = $('#ghx-detail-view span.js-epic-remove');
                        if (el.length) {
                            var key = el.attr('data-epickey');
                            var desc = el.html();
                            var classes = el.attr('class');
                            el.replaceWith('<a class="' + classes + '" href="/browse/' + key + '" title="Follow link">' + desc + '</a>');
                        }
                    }
                });
            }
        });
    }

    chrome.storage.sync.get('url', function (item) {
        if (window.location.origin === item.url) {
            makeJiraGreat();
        }
    });
})();